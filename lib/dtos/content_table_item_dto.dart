class ContentTableItemDto {
  final String id;
  final String title;
  final String coverUrl;
  final String? type;
  final int? releaseYear;

  ContentTableItemDto(
      {required this.id,
      required this.title,
      required this.coverUrl,
      this.type,
      this.releaseYear});
}
