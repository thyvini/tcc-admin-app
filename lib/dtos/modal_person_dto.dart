class ModalPersonDto {
  String name;
  String coverUrl;
  String role;

  ModalPersonDto({
    required this.name,
    required this.coverUrl,
    required this.role,
  });
}
