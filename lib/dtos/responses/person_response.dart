// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:json_annotation/json_annotation.dart';

part 'person_response.g.dart';

@JsonSerializable()
class PersonResponse {
  final String name;
  final String id;
  @JsonKey(defaultValue: "")
  final String photo;
  final String? biography;
  final String? birthDate;
  @JsonKey(defaultValue: [])
  final List<PersonWorkedOn>? workedOn;

  PersonResponse({
    required this.name,
    required this.id,
    required this.photo,
    required this.workedOn,
    this.biography,
    this.birthDate,
  });

  factory PersonResponse.fromJson(Map<String,dynamic> json) => _$PersonResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PersonResponseToJson(this);
}

@JsonSerializable()
class PersonWorkedOn {
  final PersonResponseWorkDetails work;
  final bool isMain;
  final String role;

  PersonWorkedOn({
    required this.work,
    required this.isMain,
    required this.role,
  });

  factory PersonWorkedOn.fromJson(Map<String,dynamic> json) => _$PersonWorkedOnFromJson(json);

  Map<String, dynamic> toJson() => _$PersonWorkedOnToJson(this);
}

@JsonSerializable()
class PersonResponseWorkDetails {
  final String name;
  final String id;
  final String type;
  final String cover;

  PersonResponseWorkDetails({
    required this.name,
    required this.id,
    required this.type,
    required this.cover,
  });


  factory PersonResponseWorkDetails.fromJson(Map<String,dynamic> json) => _$PersonResponseWorkDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$PersonResponseWorkDetailsToJson(this);
}
