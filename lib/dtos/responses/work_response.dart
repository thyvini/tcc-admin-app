// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:json_annotation/json_annotation.dart';

part 'work_response.g.dart';

@JsonSerializable()
class WorkResponse {
  @JsonKey(toJson: idToJson, includeIfNull: false)
  final String id;
  final String name;
  final String cover;
  final String type;

  @JsonKey(defaultValue: 0.0)
  final double averageScore;

  @JsonKey(defaultValue: [])
  final List<String> tags;

  @JsonKey(defaultValue: [])
  final List<WorkResponsePersonDetails> people;

  final int? releaseYear;
  final String? synopsis;
  final int? minuteDuration;
  final int? episodeNumber;
  final int? pageNumber;
  final String? productionCompany;
  final String? developer;
  final String? publisher;

  static idToJson(id) => id.isEmpty ? null : id;

  WorkResponse(
      {required this.id,
      required this.name,
      required this.cover,
      required this.type,
      required this.averageScore,
      required this.tags,
      required this.people,
      this.releaseYear,
      this.productionCompany,
      this.synopsis,
      this.minuteDuration,
      this.episodeNumber,
      this.pageNumber,
      this.developer,
      this.publisher});

  factory WorkResponse.fromJson(Map<String, dynamic> json) =>
      _$WorkResponseFromJson(json);

  Map<String, dynamic> toJson() => _$WorkResponseToJson(this);

  WorkResponse copyWith({
    String? id,
    String? name,
    String? cover,
    String? type,
    double? averageScore,
    List<String>? tags,
    List<WorkResponsePersonDetails>? people,
    int? releaseYear,
    String? synopsis,
    int? minuteDuration,
    int? episodeNumber,
    int? pageNumber,
    String? productionCompany,
    String? developer,
    String? publisher,
  }) {
    return WorkResponse(
      id: id ?? this.id,
      name: name ?? this.name,
      cover: cover ?? this.cover,
      type:type ?? this.type,
     averageScore: averageScore ?? this.averageScore,
      tags: tags ?? this.tags,
      people:people ?? this.people,
      releaseYear:releaseYear ?? this.releaseYear,
      synopsis:synopsis ?? this.synopsis,
      minuteDuration:minuteDuration ?? this.minuteDuration,
      episodeNumber:episodeNumber ?? this.episodeNumber,
      pageNumber:pageNumber ?? this.pageNumber,
      productionCompany: productionCompany ?? this.productionCompany,
      developer:developer ?? this.developer,
      publisher:publisher ?? this.publisher,
    );
  }
}

@JsonSerializable()
class WorkResponsePerson {
  final String id;
  @JsonKey(toJson: toNull)
  final String name;

  @JsonKey(toJson: toNull, defaultValue: '')
  final String photo;

  WorkResponsePerson({
    required this.id,
    required this.name,
    required this.photo,
  });

  static toNull(_) => null; 

  factory WorkResponsePerson.fromJson(Map<String, dynamic> json) =>
      _$WorkResponsePersonFromJson(json);

  Map<String, dynamic> toJson() => _$WorkResponsePersonToJson(this);
}

@JsonSerializable()
class WorkResponsePersonDetails {
  final WorkResponsePerson person;
  final String role;
  final bool? isMain;

  WorkResponsePersonDetails({
    this.isMain,
    required this.person,
    required this.role,
  });

  factory WorkResponsePersonDetails.fromJson(Map<String, dynamic> json) =>
      _$WorkResponsePersonDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$WorkResponsePersonDetailsToJson(this);
}
