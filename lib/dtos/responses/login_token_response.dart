import 'package:json_annotation/json_annotation.dart';

part 'login_token_response.g.dart';

@JsonSerializable()
class LoginTokenResponse {
  @JsonKey(name: 'access_token')
  final String accessToken;
  @JsonKey(name: 'refresh_token')
  final String refreshToken;

  const LoginTokenResponse(
      {required this.accessToken, required this.refreshToken});

  factory LoginTokenResponse.fromJson(Map<String, dynamic> json) => _$LoginTokenResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginTokenResponseToJson(this);
}
