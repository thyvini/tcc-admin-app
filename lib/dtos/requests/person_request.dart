// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:json_annotation/json_annotation.dart';

part 'person_request.g.dart';

@JsonSerializable()
class PersonRequest {
  final String? id;
  final String name;
  final String photo;
  final String birthDate;
  final String biography;

  const PersonRequest({ this.id, required this.name, required this.photo, required this.birthDate, required this.biography});

  factory PersonRequest.fromJson(Map<String, dynamic> json) => _$PersonRequestFromJson(json);

  Map<String, dynamic> toJson() => _$PersonRequestToJson(this);

  PersonRequest copyWith({
    String? id,
    String? name,
    String? photo,
    String? birthDate,
    String? biography,
  }) {
    return PersonRequest(
      id: id ?? this.id,
      name: name ?? this.name,
      photo: photo ?? this.photo,
      birthDate: birthDate ?? this.birthDate,
      biography: biography ?? this.biography,
    );
  }
}
