import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:tcc_admin/bloc/session_bloc.dart';
import 'package:tcc_admin/di/dependency_injection.dart';
import 'package:tcc_admin/app_router.dart';
import 'package:tcc_admin/services/auth_service.dart';

import 'helpers/custom_scroll_behavior.dart';

void main() {
  configureDependencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SessionBloc(
        authService: GetIt.I.get<AuthService>()
      ),
      child: MaterialApp(
        title: 'PAIMON Admin',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          fontFamily: 'Proxima Nova',
          unselectedWidgetColor: Colors.white
        ),
        routes: AppRouter.routes,
        scrollBehavior: CustomScrollBehaviour(),
      ),
    );
  }
}
