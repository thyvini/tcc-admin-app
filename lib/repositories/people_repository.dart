import 'package:dio/dio.dart';
import 'package:either_dart/either.dart';
import 'package:injectable/injectable.dart';
import 'package:tcc_admin/dtos/requests/person_request.dart';
import 'package:tcc_admin/exceptions/app_exception.dart';
import 'package:tcc_admin/repositories/api_repository.dart';

@singleton
class PeopleRepository extends ApiRepository {
  final Dio dio;

  PeopleRepository({required this.dio});

  Future<Either<AppException,Response>> fetchPeople({required int page}) async {
    return await requestOrError<Response>(() async {
      return await dio.get('/admin/person/list?page=$page');
    });
  }

  Future<Either<AppException,Response>> savePeople({required PersonRequest param}) async {
    return await requestOrError<Response>(() async {
      return await dio.post('/admin/person/save', data: param.toJson());
    });
  }

  Future<Either<AppException,Response>> getPerson({required String name}) async {
    return await requestOrError<Response>(() async {
      return await dio.get('/public/person/$name');
    });
  }

  Future<Either<AppException,Response>> searchPeople({required String query}) async {
    return await requestOrError<Response>(() async {
      return await dio.get('/public/person/search?page=0&input=$query');
    });
  }

  Future<Either<AppException,Response>> deletePerson({required String id}) async {
    return await requestOrError<Response>(() async {
      return await dio.delete('/admin/person/delete?id=$id');
    });
  }
}
