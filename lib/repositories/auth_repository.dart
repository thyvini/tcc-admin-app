import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:tcc_admin/dtos/requests/login_request.dart';
import 'package:tcc_admin/dtos/responses/login_token_response.dart';
import 'package:tcc_admin/repositories/api_repository.dart';
import 'package:tcc_admin/repositories/local_device_repository.dart';

@singleton
class AuthRepository extends ApiRepository{
  final Dio dio;
  final LocalDeviceRepository localRepository;

  AuthRepository({required this.dio, required this.localRepository});

  Future<LoginTokenResponse?> login({required LoginRequest loginParams}) {
    return requestOrNull(() async {
      final request = await dio.post(
        '/public/login',
        data: loginParams.toJson(),
        options: Options(contentType: Headers.formUrlEncodedContentType),
      );

      return LoginTokenResponse.fromJson(request.data);
    });
  }

  Future<String?> verifyToken() async {
    if (await localRepository.containsKey(bearerTokenKey)) {
      final String? bearerToken = await localRepository.getBearerToken();

      if (!JwtDecoder.isExpired(bearerToken ?? '')) {
        return bearerToken;
      } else {
        try {
          final refreshToken = await localRepository.getRefreshToken();
          final Response renewTokenResponse = await dio.get(
              'https://tcc-b-e.herokuapp.com/public/token/refresh',
              options:
                  Options(headers: {'Authorization': 'Bearer $refreshToken'}));

          await localRepository
              .setRefreshToken(renewTokenResponse.data['access_token']);

          return (renewTokenResponse.data['access_token']);
        } catch (e) {
          return null;
        }
      }
    }
    return null;
  }

}
