import 'package:dio/dio.dart';
import 'package:either_dart/either.dart';
import 'package:injectable/injectable.dart';
import 'package:tcc_admin/dtos/responses/work_response.dart';
import 'package:tcc_admin/exceptions/app_exception.dart';
import 'package:tcc_admin/repositories/api_repository.dart';

@singleton
class WorkRepository extends ApiRepository {
  final Dio dio;

  WorkRepository({required this.dio});

  Future<Either<AppException,Response>> fetchWorks({int page =0}) async {
    return await requestOrError<Response>(() async {
      return await dio.get('/public/work/list?page=$page');
    });
  }

  // Future<Either<AppException,Response>> savePeople({required PersonRequest param}) async {
  //   return await requestOrError<Response>(() async {
  //     return await dio.post('/admin/works/save', data: param.toJson());
  //   });
  // }

  Future<Either<AppException,Response>> getWorks({required String id}) async {
    return await requestOrError<Response>(() async {
      return await dio.get('/public/works/$id');
    });
  }

  Future<Either<AppException,Response>> searchWorks({required String query}) async {
    return await requestOrError<Response>(() async {
      return await dio.get('/public/work/search?page=0&type=any&input=$query');
    });
  }

  Future<Either<AppException,Response>> deleteWork({required String id}) async {
    return await requestOrError<Response>(() async {
      return await dio.delete('/admin/work/delete?id=$id');
    });
  }

  Future<Either<AppException,Response>> saveWork({required WorkResponse work}) async {
    return await requestOrError<Response>(() async {
      return await dio.post('/admin/work/save', data: work.toJson());
    });
  }

  Future<Either<AppException,Response>> getWork({required String id}) async {
    return await requestOrError<Response>(() async {
      return await dio.get('/public/work?id=$id');
    });
  }
}
