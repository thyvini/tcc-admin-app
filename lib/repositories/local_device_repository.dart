import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String bearerTokenKey = 'bearerToken';
const String refreshTokenKey = 'refreshToken';
const String currentUserKey = 'currentUser';

@singleton
class LocalDeviceRepository {
  final SharedPreferences storage;

  LocalDeviceRepository(this.storage);

  Future<void> setBearerToken(String bearerToken) async =>
      await storage.setString( bearerTokenKey,  bearerToken);

  Future<String?> getBearerToken() async =>
      await storage.getString( bearerTokenKey);

  Future<void> setRefreshToken(String refreshToken) async =>
      await storage.setString( refreshTokenKey,  refreshToken);

  Future<String?> getRefreshToken() async => storage.getString( refreshTokenKey);

  Future<void> setCurrentUser(String username) async =>
      await storage.setString( currentUserKey,  username);

  Future<String?> getCurrentUser() async =>
      await storage.getString( currentUserKey);

  Future<void> clear() async => await storage.clear();

  Future<bool> containsKey(String key) async =>
      await storage.containsKey( key);
}
