import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:either_dart/either.dart';
import 'package:tcc_admin/exceptions/app_exception.dart';
import 'package:tcc_admin/helpers/toast.dart';

class ApiRepository {
  Either<AppException, T> handleError<T>(
      DioError error, StackTrace? stackTrace) {
    if (error.response != null) {
      if (error.response?.data != null &&
          error.response?.data.runtimeType == String) {
        final data =
            error.response?.data != null && error.response?.data.isNotEmpty
                ? json.decode(error.response?.data)
                : {};

        return Left(AppException.fromMap(data));
      }
      return Left(AppException.fromMap(error.response?.data));
    } else if (error.type == DioErrorType.cancel) {
      return Left(AppException.canceled());
    }

    return Left(AppException.undefined());
  }

  Future<T?> requestOrNull<T>(Future<T?> Function() req) async {
    try {
      return await req();
    } on DioError catch (_) {
      return null;
    }
  }

  Future<Either<AppException, T>> requestOrError<T>(
      Future<T?> Function() req) async {
    try {
      final response = await req();
      if (response != null) return Right(response);
      return Left(AppException("404", "Não encontrado"));
    } on DioError catch (e, stackTrace) {
      showToast(message: e.response?.data?["error_message"]);

      return handleError(e, stackTrace);
    }
  }
}
