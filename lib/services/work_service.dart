import 'package:either_dart/either.dart';
import 'package:injectable/injectable.dart';
import 'package:tcc_admin/dtos/responses/work_response.dart';
import 'package:tcc_admin/exceptions/app_exception.dart';
import 'package:tcc_admin/repositories/work_repository.dart';

@singleton
class WorkService {
  final WorkRepository workRepository;

  WorkService({required this.workRepository});

  Future<Either<AppException, List<WorkResponse>>> getWorks({int page = 0}) async {
    return await workRepository.fetchWorks(page: page).thenRight((response) {
      if (response.data == null) {
        return Left(AppException('404', 'Erro'));
      }

      return Right(
       response.data.map<WorkResponse>((item) => WorkResponse.fromJson(item)).toList(),
      );
    });
  } 

  Future<Either<AppException, List<WorkResponse>>> searchWorks({required String query}) async {
    return await workRepository.searchWorks(query: query).thenRight((response) {
      if (response.data == null) {
        return Left(AppException('404', 'Erro'));
      }

      return Right(
       response.data.map<WorkResponse>((item) => WorkResponse.fromJson(item)).toList(),
      );
    });
  } 

  Future<Either<AppException, bool>> deleteWork({required String id}) async {
    return await workRepository.deleteWork(id: id).thenRight((response) {
      if (response.data == null) {
        return Left(AppException('404', 'Erro'));
      }

      return const Right(true);
    });
  } 

  Future<Either<AppException, bool>> saveWork({required WorkResponse work}) async {
    return await workRepository.saveWork(work: work).thenRight((response) {
      if (response.data == null) {
        return Left(AppException('404', 'Erro'));
      }

      return const Right(true);
    });
  } 

  Future<Either<AppException, WorkResponse>> getWork({required String id}) async {
    return await workRepository.getWork(id: id).thenRight((response) {
      if (response.data == null) {
        return Left(AppException('404', 'Erro'));
      }
      
      final work = WorkResponse.fromJson(response.data!);

      work.people.sort((a, b) {
        final aCond = a.isMain ?? false;
        final bCond = b.isMain ?? false;

        if (aCond && !bCond) {
          return -1;
        }
        if (!aCond && bCond) {
          return 1;
        }
        return 0;
      });

      return Right(work);
    });
  } 
}
