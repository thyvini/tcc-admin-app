import 'package:injectable/injectable.dart';
import 'package:tcc_admin/dtos/requests/login_request.dart';
import 'package:tcc_admin/dtos/responses/login_token_response.dart';
import 'package:tcc_admin/repositories/auth_repository.dart';
import 'package:tcc_admin/repositories/local_device_repository.dart';

@singleton
class AuthService {
  final AuthRepository authRepository;
  final LocalDeviceRepository localRepository;

  AuthService({required this.authRepository, required this.localRepository});

  Future<LoginTokenResponse?> loginWithPassword(
      {required String username, required String password}) async {
    final loginToken = await authRepository.login(
        loginParams: LoginRequest(username: username, password: password));
    if (loginToken != null) {
      await Future.wait([
        localRepository.setBearerToken(loginToken.accessToken),
        localRepository.setRefreshToken(loginToken.refreshToken),
        localRepository.setCurrentUser(username)
      ]);
    }
    return loginToken;
  }

  Future<String?> getCurrentUser() async =>
      await localRepository.getCurrentUser();

  Future<String?> verifyToken() async => await authRepository.verifyToken();

  Future<void> logout() async => await localRepository.clear();
}
