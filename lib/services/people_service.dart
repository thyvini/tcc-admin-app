import 'package:either_dart/either.dart';
import 'package:injectable/injectable.dart';
import 'package:tcc_admin/dtos/requests/person_request.dart';
import 'package:tcc_admin/dtos/responses/person_response.dart';
import 'package:tcc_admin/exceptions/app_exception.dart';
import 'package:tcc_admin/repositories/people_repository.dart';


@singleton
class PeopleService {
  final PeopleRepository peopleRepository;

  PeopleService({required this.peopleRepository});

  Future<Either<AppException, List<PersonResponse>>> getPeople({int page = 0}) async {
    return await peopleRepository.fetchPeople(page: page).thenRight((response) {
      if (response.data == null) {
        return Left(AppException('404', 'Erro'));
      }

      return Right(
        response.data
            .map<PersonResponse>((item) => PersonResponse.fromJson(item))
            .toList(),
      );
    });
  }

  Future<Either<AppException, PersonResponse>> getPerson({required String name}) async {
    return await peopleRepository.getPerson(name: name).thenRight((response) {
      if (response.data == null) {
        return Left(AppException('404', 'Erro'));
      }

      return Right(PersonResponse.fromJson(response.data));
    });
  }

  Future<Either<AppException, bool>> savePeople({required PersonRequest person}) async {
    return await peopleRepository.savePeople(param: person).thenRight((response) {
      if (response.data == null) {
        return Left(AppException('404', 'Erro'));
      }

      return const Right(true);
    });
  }

  Future<Either<AppException, List<PersonResponse>>> searchPeople({required String query}) async {
    return await peopleRepository.searchPeople(query: query).thenRight((response) {
      if (response.data == null) {
        return Left(AppException('404', 'Erro'));
      }

      return Right(
        response.data
            .map<PersonResponse>((item) => PersonResponse.fromJson(item))
            .toList(),
      );
    });
  }

  Future<Either<AppException, bool>> deletePerson({required String id}) async {
    return await peopleRepository.deletePerson(id: id).thenRight((response) {
      if (response.data == null) {
        return Left(AppException('404', 'Erro'));
      }

      return const Right(true);
    });
  }
}
