import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:tcc_admin/repositories/local_device_repository.dart';

@module
abstract class DioRegister {
  @lazySingleton
  Dio dio(LocalDeviceRepository localRepository) {
    final dio = Dio(BaseOptions(
      baseUrl: 'https://tcc-b-e.herokuapp.com',
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
        "Access-Control-Allow-Methods": "GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS"
      },
    ));
    dio.interceptors.add(InterceptorsWrapper(onRequest:
        (RequestOptions options, RequestInterceptorHandler handler) async {
      if (!options.path.contains('/public')) {
        final bearerToken = await localRepository.getBearerToken();
        options.headers['Authorization'] = "Bearer $bearerToken";
      }
      return handler.next(options);
    }, onError: (DioError error, ErrorInterceptorHandler handler) async {
      if (error.response?.statusCode == 403 &&
          await localRepository.containsKey(refreshTokenKey)) {
        await _refreshToken(localRepository, dio);
        return handler.resolve(await _retry(error.requestOptions, dio));
      }
      return handler.next(error);
    }));

    return dio;
  }
}

Future<Response<dynamic>> _retry(RequestOptions requestOptions, Dio dio) async {
  final options = Options(
    method: requestOptions.method,
    headers: requestOptions.headers,
  );
  return dio.request<dynamic>(requestOptions.path,
      data: requestOptions.data,
      queryParameters: requestOptions.queryParameters,
      options: options);
}

Future<void> _refreshToken(
    LocalDeviceRepository localRepository, Dio dio) async {
  final String? refreshToken = await localRepository.getRefreshToken();
  final response = await dio.get('/public/token/refresh',
      options: Options(
          contentType: Headers.jsonContentType,
          headers: {"Authorization": "Bearer $refreshToken"}));
  if (response.statusCode == 200) {
    localRepository.setBearerToken(response.data['access_token']);
  }
}
