// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i5;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i3;

import '../repositories/auth_repository.dart' as _i10;
import '../repositories/local_device_repository.dart' as _i4;
import '../repositories/people_repository.dart' as _i6;
import '../repositories/work_repository.dart' as _i8;
import '../services/auth_service.dart' as _i11;
import '../services/people_service.dart' as _i7;
import '../services/work_service.dart' as _i9;
import 'register_dio.dart' as _i13;
import 'register_flutter_secure_storage.dart'
    as _i12; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  final registerSharedPreferences = _$RegisterSharedPreferences();
  final dioRegister = _$DioRegister();
  gh.singletonAsync<_i3.SharedPreferences>(
      () => registerSharedPreferences.sharedPreferences());
  gh.singletonAsync<_i4.LocalDeviceRepository>(() async =>
      _i4.LocalDeviceRepository(await get.getAsync<_i3.SharedPreferences>()));
  gh.lazySingletonAsync<_i5.Dio>(() async =>
      dioRegister.dio(await get.getAsync<_i4.LocalDeviceRepository>()));
  gh.singletonAsync<_i6.PeopleRepository>(
      () async => _i6.PeopleRepository(dio: await get.getAsync<_i5.Dio>()));
  gh.singletonAsync<_i7.PeopleService>(() async => _i7.PeopleService(
      peopleRepository: await get.getAsync<_i6.PeopleRepository>()));
  gh.singletonAsync<_i8.WorkRepository>(
      () async => _i8.WorkRepository(dio: await get.getAsync<_i5.Dio>()));
  gh.singletonAsync<_i9.WorkService>(() async => _i9.WorkService(
      workRepository: await get.getAsync<_i8.WorkRepository>()));
  gh.singletonAsync<_i10.AuthRepository>(() async => _i10.AuthRepository(
        dio: await get.getAsync<_i5.Dio>(),
        localRepository: await get.getAsync<_i4.LocalDeviceRepository>(),
      ));
  gh.singletonAsync<_i11.AuthService>(() async => _i11.AuthService(
        authRepository: await get.getAsync<_i10.AuthRepository>(),
        localRepository: await get.getAsync<_i4.LocalDeviceRepository>(),
      ));
  return get;
}

class _$RegisterSharedPreferences extends _i12.RegisterSharedPreferences {}

class _$DioRegister extends _i13.DioRegister {}
