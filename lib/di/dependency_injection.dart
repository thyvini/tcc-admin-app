import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:tcc_admin/di/dependency_injection.config.dart';

final getIt = GetIt.instance;

@InjectableInit(initializerName: r'$initGetIt')
void configureDependencies() => $initGetIt(getIt);
