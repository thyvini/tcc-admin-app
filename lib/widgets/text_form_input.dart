import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFormInput extends StatelessWidget {
  final String? hintText;
  final String? helperText;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final String? Function(String?)? validator;
  final bool? obscureText;
  final bool? enabled;
  final bool isRequired;
  final List<TextInputFormatter>? formatters;
  final TextInputAction? textInputAction;
  final TextInputType? keyboardType;
  final Function(String?)? onChanged;
  final String? errorText;
  final Widget? suffixIcon;

  const TextFormInput({
    Key? key,
    this.hintText,
    this.validator,
    this.obscureText,
    this.controller,
    this.formatters,
    this.keyboardType,
    this.isRequired = false,
    this.textInputAction,
    this.enabled = true,
    this.focusNode,
    this.onChanged,
    this.errorText,
    this.helperText,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      focusNode: focusNode,
      obscureText: obscureText ?? false,
      cursorColor: Colors.white,
      cursorWidth: 1.0,
      cursorHeight: 20,
      enabled: enabled,
      style: const TextStyle(
        color: Colors.white,
        letterSpacing: 1.25,
        fontSize: 20.0,
      ),
      decoration: InputDecoration(
        errorText: errorText,
        // isCollapsed: true,
        labelStyle: const TextStyle(color: Colors.white38, letterSpacing: 1.25),
        helperText: helperText,
        labelText: hintText,
        alignLabelWithHint: true,        
        contentPadding:
            const EdgeInsets.symmetric(vertical: 5.0, horizontal: 1),
        enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: 0.5)),
        focusedBorder: const UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: 1)),
        border: const UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white)),
        suffixIcon: suffixIcon,
      ),
      textInputAction: textInputAction,
      keyboardType: keyboardType,
      inputFormatters: formatters,
      onChanged: onChanged,
      validator: (value) {
        if ((value == null || value.isEmpty) && isRequired) {
          return 'Campo Obrigatório';
        }

        if (validator != null) {
          validator!(value);
        }
      },
    );
  }
}
