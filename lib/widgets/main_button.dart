import 'package:flutter/material.dart';

class MainButton extends StatelessWidget {
  final VoidCallback? onTap;
  final Widget child;
  final bool? loading;

  const MainButton({
    Key? key,
    this.onTap,
    required this.child,
    this.loading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return loading ?? false
        ? const SizedBox(
            width: 27.0,
            height: 27.0,
            child: CircularProgressIndicator(
              color: Colors.white,
            ),
          )
        : Material(
            color: Colors.transparent,
            child: Ink(
              decoration: BoxDecoration(
                color: const Color(0x36000000),
                border: Border.all(color: Colors.white, width: 2.0),
                borderRadius: BorderRadius.circular(80.0),
              ),
              child: InkWell(
                splashColor: const Color(0x963B3065),
                borderRadius: BorderRadius.circular(80.0),
                onTap: onTap ?? () {},
                child: Container(
                  width: double.infinity,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 7.0),
                  child: child,
                ),
              ),
            ),
          );
  }
}
