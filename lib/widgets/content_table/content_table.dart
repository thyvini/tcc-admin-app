import 'package:flutter/material.dart';
import 'package:tcc_admin/constants/helpers.dart';
import 'package:tcc_admin/dtos/content_table_item_dto.dart';
import 'package:tcc_admin/widgets/image_widget.dart';

class ContentTable extends StatelessWidget {
  final List<ContentTableItemDto> items;
  final bool canLoadMore;
  final void Function(ContentTableItemDto) onItemEditTap;
  final void Function(ContentTableItemDto) onItemDeleteTap;
  final void Function() onEndOfList;
  final bool isLoadingMore;

  final ScrollController scrollController = ScrollController();

  ContentTable(
      {required this.items,
      required this.onItemEditTap,
      required this.onItemDeleteTap,
      required this.onEndOfList,
      this.isLoadingMore = false,
      this.canLoadMore = false,
      Key? key})
      : super(key: key) {
    if (canLoadMore) {
      scrollController.addListener(() {
        if (scrollController.position.atEdge) {
          if (scrollController.position.pixels != 0) {
            onEndOfList();
          }
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return ListView.separated(
        itemCount: items.length + (isLoadingMore ? 2 : 1),
        controller: scrollController,
        separatorBuilder: (context, index) =>
            const Divider(color: Colors.white, height: .5),
        padding: EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.15),
        itemBuilder: (context, index) {
          if (index == 0) {
            final hasTypeAndYear =
                items.first.type != null && items.first.releaseYear != null;

            return Padding(
              padding: EdgeInsets.symmetric(vertical: 14.0),
              child: Row(
                children: [
                  text('#', align: TextAlign.center),
                  hasTypeAndYear
                      ? text('Título')
                      : const Spacer(),
                  hasTypeAndYear
                      ? const Spacer(flex: 6)
                      : const SizedBox(
                          width: 12.0,
                        ),
                  hasTypeAndYear ? text('Tipo') : text('Nome'),
                  const Spacer(flex: 2)
                ],
              ),
            );
          }

          if (index == items.length + 1 && isLoadingMore) {
            return const Padding(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              child: Center(child: CircularProgressIndicator()),
            );
          }
          final ContentTableItemDto item = items[index - 1];
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  (index).toString(),
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 20.0, color: Colors.white),
                ),
              ),
              if (item.type != null || item.releaseYear != null)
                Expanded(
                  flex: 7,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ImageWidget(fit: BoxFit.cover, url: item.coverUrl),
                        Padding(
                          padding: const EdgeInsets.only(left: 12.0),
                          child: SizedBox(
                            height: 100,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Text(
                                    item.title,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                        fontSize: 16.0),
                                  ),
                                ),
                                const Spacer(),
                                Expanded(
                                  child: Text(
                                      item.releaseYear?.toString() ?? '',
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                          color: Colors.white)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              else
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ImageWidget(url: item.coverUrl),
                  ),
                ),
              const SizedBox(width: 12.0),
              Expanded(
                child: Text(
                  item.type == null || item.releaseYear == null
                      ? item.title
                      : translateWorkType[item.type!],
                  textAlign: TextAlign.start,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
              ),
              Expanded(
                child: InkWell(
                  hoverColor: Colors.transparent,
                  child: const Icon(
                    Icons.edit_note,
                    color: Colors.lightBlue,
                    size: 24,
                  ),
                  onTap: () => onItemEditTap(item),
                ),
              ),
              Expanded(
                child: InkWell(
                  hoverColor: Colors.transparent,
                  child: const Icon(
                    Icons.delete,
                    color: Colors.red,
                    size: 24,
                  ),
                  onTap: () => onItemDeleteTap(item),
                ),
              ),
            ],
          );
        },
      );
    });
  }
}

final List<TableRow> linhas = [];
Widget text(String content, {TextAlign align = TextAlign.start}) => Expanded(
      child: Text(
        content,
        textAlign: align,
        style: const TextStyle(
            fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
      ),
    );
