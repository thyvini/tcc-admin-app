import 'package:flutter/material.dart';

class ImageWidget extends StatelessWidget {
  final String url;
  final BoxFit? fit;
  
  const ImageWidget({required this.url, this.fit, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      url,
      filterQuality: FilterQuality.none,
      fit: fit,
      width: 60,
      height: 95,
      errorBuilder: (context, error, stackTrace) => const SizedBox(
        width: 60,
        height: 95,
        child: Icon(
          Icons.hide_image,
          color: Colors.white,
        ),
      ),
    );
  }
}
