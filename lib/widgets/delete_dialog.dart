import 'package:flutter/material.dart';

Future<bool?> showDeleteDialog({
  required BuildContext context,
  required String name,
}) {
  return showDialog(
    context: context,
    builder: (context) {
      return LayoutBuilder(builder: (context, constraints) {
        return Dialog(
          insetPadding: EdgeInsets.symmetric(
              vertical: constraints.maxHeight / 3,
              horizontal: constraints.maxWidth / 3),
          child: Padding(
            padding: const EdgeInsets.all(35.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  'Atenção!!!',
                  style: TextStyle(
                      color: Theme.of(context).errorColor, fontSize: 30.0),
                ),
                const SizedBox(height: 32.0),
                Text(
                  'Deseja apagar $name?',
                  style: const TextStyle(fontSize: 24.0),
                ),
                const SizedBox(height: 15.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                      onPressed: () => Navigator.pop(context, false),
                      child: Text(
                        'Não',
                        style: TextStyle(
                            color: Theme.of(context).errorColor,
                            fontSize: 22.0),
                      ),
                    ),
                    OutlinedButton(
                      onPressed: () => Navigator.pop(context, true),
                      style: OutlinedButton.styleFrom(
                        backgroundColor: Colors.green.shade900,
                      ),
                      child: const Text(
                        'Sim',
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      });
    },
  );
}
