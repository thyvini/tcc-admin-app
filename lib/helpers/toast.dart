import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void showToast({required String? message}) {
  Fluttertoast.showToast(
    msg: message ?? 'Erro inesperado',
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 5,
    textColor: Colors.white,
    webBgColor: 'red',
    fontSize: 14.0,
  );
}