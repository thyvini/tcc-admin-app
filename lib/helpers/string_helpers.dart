String formatDateToApi(String date){
  final List<String> splitStr = date.split('/');

  return '${splitStr.last}-${splitStr[1]}-${splitStr.first}';
}

String formatDateToUi(String date){
  final List<String> splitStr = date.split('-');

  return '${splitStr.last}/${splitStr[1]}/${splitStr.first}';
}