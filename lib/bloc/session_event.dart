part of 'session_bloc.dart';

abstract class SessionEvent {}

class LoginUserEvent extends SessionEvent {
  final String username;
  final String password;

  LoginUserEvent({
    required this.username,
    required this.password,
  });
}

class LogoutUserEvent extends SessionEvent {}

class VerifyUserSessionEvent extends SessionEvent {}
