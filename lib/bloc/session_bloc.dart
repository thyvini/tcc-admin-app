import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:tcc_admin/dtos/responses/login_token_response.dart';
import 'package:tcc_admin/exceptions/app_exception.dart';
import 'package:tcc_admin/helpers/toast.dart';
import 'package:tcc_admin/services/auth_service.dart';

part 'session_event.dart';
part 'session_state.dart';

class SessionBloc extends Bloc<SessionEvent, SessionState> {
  final AuthService authService;

  SessionBloc({required this.authService}) : super(SessionInitial()) {
    on<LoginUserEvent>((event, emit) => _loginUser(event, emit));
    // on<LogoutUserEvent>((event, emit) => _logoutUser(event, emit));
    // on<RegisterUserEvent>((event, emit) => _registerUser(event, emit));
    on<VerifyUserSessionEvent>((event, emit) => _verifyUserSession(emit));

    add(VerifyUserSessionEvent());
  }

  void _loginUser(LoginUserEvent event, Emitter<SessionState> emit) async {
    try {
      emit(SessionLoadingState());

      final LoginTokenResponse? token = await authService.loginWithPassword(
        username: event.username,
        password: event.password,
      );
      if(token != null){
        emit(SessionUpState(token: token));
      }else{
        emit(SessionErrorState(message: 'Usuário inexistente'));
        showToast(message: (state as SessionErrorState).message);
      }
    } on AppException catch (e) {
      emit(SessionErrorState(message: e.message));
    }
  }

  void _verifyUserSession(Emitter<SessionState> emit) async {
    try {
      emit(SessionLoadingState());
      final String? userToken = await authService.verifyToken();
      if (userToken != null) {
        final token = JwtDecoder.decode(userToken);

        emit(
          SessionUpState(
            token: LoginTokenResponse(
              accessToken: userToken,
              refreshToken: '',
            ),
          ),
        );
      } else {
        emit(SessionDownState());
        showToast(message: 'Deslogado');
      }
    } on AppException catch (e) {
      emit(SessionErrorState(message: e.message));
    }
  }
}
