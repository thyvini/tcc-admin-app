// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'session_bloc.dart';

abstract class SessionState {}

class SessionInitial extends SessionState {}

class SessionUpState extends SessionState {
  final LoginTokenResponse? token;

  SessionUpState({
    required this.token,
  });
}

class SessionDownState extends SessionState {}

class SessionLoadingState extends SessionState {}

class SessionErrorState extends SessionState {
  final String message;

  SessionErrorState({
    required this.message,
  });
}
