class AppException implements Exception {
  final String code;
  final String message;

  AppException(this.code, this.message);

  AppException.fromMap(Map map):
    code = map['code'] ?? map['error'] ?? 'XX',
    message = map['error_message'] ?? 'undefined';

  AppException.undefined(): 
    code = 'XX',
    message = 'Erro inesperado';

  AppException.canceled(): 
    code = 'XX',
    message = 'canceled';
}
