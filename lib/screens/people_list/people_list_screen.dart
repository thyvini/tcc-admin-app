import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/app_router.dart';
import 'package:tcc_admin/dtos/content_table_item_dto.dart';
import 'package:tcc_admin/screens/people_list/bloc/people_list_bloc.dart';
import 'package:tcc_admin/widgets/content_table/content_table.dart';
import 'package:tcc_admin/widgets/delete_dialog.dart';

class PeopleListScreen extends StatelessWidget {
  const PeopleListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PeopleListBloc, PeopleListState>(
        builder: (context, state) {
      if (state is InitialPeopleListState || state is LoadingPeopleListState) {
        return const Center(child: CircularProgressIndicator());
      }

      if (state is SuccessPeopleListState ||
          state is LoadingMorePeopleListState ||
          state is SuccessSearchPeopleListState) {

        final items = state is SuccessSearchPeopleListState
            ? state.searchPeople!
                .map((person) => ContentTableItemDto(
                      id: person.id,
                      title: person.name,
                      coverUrl: person.photo,
                    ))
                .toList()
            : state.people
                .map((person) => ContentTableItemDto(
                      id: person.id,
                      title: person.name,
                      coverUrl: person.photo,
                    ))
                .toList();

        if(items.isEmpty){
          return Center(child: Text('Nenhuma pessoa encontrada', style: TextStyle(color: Colors.white)));
        }

        return ContentTable(
          onItemEditTap: (item) {
            Navigator.pushNamed(context, AppRouter.maintainPerson,
                arguments: item.title);
          },
          onItemDeleteTap: (item) async{
            final bool? delete = await showDeleteDialog(context: context, name: item.title);

            if (delete ?? false) {
              context.read<PeopleListBloc>().add(DeletePersonEvent(item.id));
            }
          },
          onEndOfList: () =>
              context.read<PeopleListBloc>().add(LoadNextPageEvent()),
          isLoadingMore: state is LoadingMorePeopleListState,
          canLoadMore: state is SuccessPeopleListState,
          items: items,
        );
      }

      return Container();
    });
  }
}
