import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/di/dependency_injection.dart';
import 'package:tcc_admin/dtos/responses/person_response.dart';
import 'package:tcc_admin/services/people_service.dart';

part 'people_list_event.dart';
part 'people_list_state.dart';

class PeopleListBloc extends Bloc<PeopleListEvent, PeopleListState> {
  late final PeopleService _peopleService;
  PeopleListBloc() : super(InitialPeopleListState()) {
    _peopleService = getIt.get<PeopleService>();
    on<LoadPeopleListEvent>((event, emit) async => _loadPeopleList(emit));
    on<LoadNextPageEvent>((event, emit) async => _loadNexPagePeopleList(emit));
    on<SearchPeopleEvent>((event, emit) async => _searchPeople(event, emit));
    on<DeletePersonEvent>((event, emit) async => _deletePerson(event, emit));

    add(LoadPeopleListEvent());
  }

  void _loadPeopleList(Emitter<PeopleListState> emit) async {
    emit(LoadingPeopleListState());
    final peopleList = await _peopleService.getPeople();
    peopleList.fold(
      (error) => emit(ErrorPeopleListState(error.message)),
      (people) => emit(SuccessPeopleListState(people: people)),
    );
  }

  void _loadNexPagePeopleList(Emitter<PeopleListState> emit) async {
    if (state is LoadingMorePeopleListState || state is LoadingPeopleListState)
      return;

    final int page = state.page + 1;
    emit(LoadingMorePeopleListState(people: state.people, page: page));
    final peopleList = await _peopleService.getPeople(page: page);

    peopleList.fold(
      (error) =>
          emit(SuccessPeopleListState(people: state.people, page: page - 1)),
      (people) => emit(SuccessPeopleListState(
          people: [...state.people, ...people], page: page)),
    );
  }

  void _searchPeople(
      SearchPeopleEvent event, Emitter<PeopleListState> emit) async {
    if (event.query.isEmpty) {
      emit(LoadingPeopleListState(page: 0));
      final peopleList = await _peopleService.getPeople();
      peopleList.fold(
        (error) => emit(ErrorPeopleListState(error.message)),
        (people) => emit(SuccessPeopleListState(people: people)),
      );

      return;
    }

    emit(LoadingPeopleListState(page: state.page, people: state.people));

    final peopleList = await _peopleService.searchPeople(query: event.query);

    peopleList.fold(
      (error) =>
          emit(SuccessPeopleListState(people: state.people, page: state.page)),
      (people) => emit(SuccessSearchPeopleListState(
        people: state.people,
        page: state.page,
        searchPeople: people,
      )),
    );
  }

  void _deletePerson(
      DeletePersonEvent event, Emitter<PeopleListState> emit) async {
    emit(LoadingPeopleListState());

    final deletePerson = await _peopleService.deletePerson(id: event.id);

    deletePerson.fold((left) => emit(ErrorPeopleListState(left.message)), (right) => null);

    final peopleList = await _peopleService.getPeople();
    peopleList.fold(
      (error) => emit(ErrorPeopleListState(error.message)),
      (people) => emit(SuccessPeopleListState(people: people)),
    );
  }
}
