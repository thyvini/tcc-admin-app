part of 'people_list_bloc.dart';

abstract class PeopleListEvent {}

class LoadPeopleListEvent extends PeopleListEvent {}

class LoadNextPageEvent extends PeopleListEvent{}

class SearchPeopleEvent extends PeopleListEvent{
  final String query;

  SearchPeopleEvent(this.query);
}
class DeletePersonEvent extends PeopleListEvent{
  final String id;

  DeletePersonEvent(this.id);
}