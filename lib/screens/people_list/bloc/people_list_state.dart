part of 'people_list_bloc.dart';

abstract class PeopleListState {
  final List<PersonResponse> people;
  final int page;

  PeopleListState({
    this.people = const [],
    this.page = 0,
  });
}

class InitialPeopleListState extends PeopleListState {}

class LoadingPeopleListState extends PeopleListState {
  LoadingPeopleListState({super.page, super.people});
}

class LoadingMorePeopleListState extends PeopleListState {
  LoadingMorePeopleListState({required super.page, super.people});
}

class ErrorPeopleListState extends PeopleListState {
  final String message;

  ErrorPeopleListState(this.message);
}

class SuccessPeopleListState extends PeopleListState {
  SuccessPeopleListState({required super.people, super.page});
}

class SuccessSearchPeopleListState extends PeopleListState {
  final List<PersonResponse>? searchPeople;
  
  SuccessSearchPeopleListState({this.searchPeople, super.people, super.page});
}
