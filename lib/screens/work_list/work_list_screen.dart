import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/app_router.dart';
import 'package:tcc_admin/dtos/content_table_item_dto.dart';
import 'package:tcc_admin/screens/work_list/bloc/work_list_bloc.dart';
import 'package:tcc_admin/widgets/content_table/content_table.dart';
import 'package:tcc_admin/widgets/delete_dialog.dart';

class WorkListScreen extends StatelessWidget {
  const WorkListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WorkListBloc, WorkListState>(builder: (context, state) {
      if (state is InitialWorkListState || state is LoadingWorkListState) {
        return const Center(child: CircularProgressIndicator());
      }

      if (state is SuccessWorkListState ||
          state is LoadingMoreWorksListState ||
          state is SuccessSearchWorksListState) {
        final items = state.works
            .map(
              (work) => ContentTableItemDto(
                id: work.id,
                title: work.name,
                coverUrl: work.cover,
                type: work.type,
                releaseYear: work.releaseYear,
              ),
            )
            .toList();

        if (items.isEmpty) {
          return const Center(
              child: Text('Nenhuma pessoa encontrada',
                  style: TextStyle(color: Colors.white)));
        }

        return ContentTable(
          onItemEditTap: (item) {
            Navigator.pushNamed(context, AppRouter.maintainWork, arguments: item.id);
          },
          onItemDeleteTap: (item) async {
            final bool? delete = await showDeleteDialog(context: context, name: item.title);

            if (delete ?? false) {
              context.read<WorkListBloc>().add(DeleteWorkEvent(item.id));
            }
          },
          onEndOfList: () =>
              context.read<WorkListBloc>().add(LoadNextPageEvent()),
          isLoadingMore: state is LoadingMoreWorksListState,
          canLoadMore: state is SuccessWorkListState,
          items: items,
        );
      }

      return Container();
    });
  }
}
