part of 'work_list_bloc.dart';

abstract class WorkListState {
  final List<WorkResponse> works;
  final int page;

  WorkListState({
    this.works = const [],
    this.page = 0,
  });
}

class InitialWorkListState extends WorkListState {}

class LoadingWorkListState extends WorkListState {
  LoadingWorkListState({super.page});
}

class LoadingMoreWorksListState extends WorkListState {
  LoadingMoreWorksListState({required super.page, super.works});
}

class ErrorWorkListState extends WorkListState {
  final String message;

  ErrorWorkListState(this.message);
}

class SuccessWorkListState extends WorkListState {
  SuccessWorkListState({required super.works, super.page});
}

class SuccessSearchWorksListState extends WorkListState {
  SuccessSearchWorksListState({
    required super.works,
  });
}