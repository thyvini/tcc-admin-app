import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/di/dependency_injection.dart';
import 'package:tcc_admin/dtos/responses/work_response.dart';
import 'package:tcc_admin/services/work_service.dart';

part 'work_list_event.dart';
part 'work_list_state.dart';

class WorkListBloc extends Bloc<WorkListEvent, WorkListState> {
  late final WorkService _workService;
  WorkListBloc() : super(InitialWorkListState()) {
    _workService = getIt.get<WorkService>();
    on<LoadWorkListEvent>((event, emit) => _loadWorkList(emit));
    on<LoadNextPageEvent>((event, emit) => _loadNextPageWorkList(emit));
    on<SearchWorksEvent>((event, emit) => _searchWorks(event, emit));
    on<DeleteWorkEvent>((event, emit) async => _deleteWork(event, emit));


    add(LoadWorkListEvent());
  }

  void _loadWorkList(Emitter<WorkListState> emit) async {
    emit(LoadingWorkListState());
    final workList = await _workService.getWorks();
    workList.fold(
      (error) => emit(ErrorWorkListState(error.message)),
      (works) => emit(SuccessWorkListState(works: works)),
    );
  }

  void _loadNextPageWorkList(Emitter<WorkListState> emit) async {
    if (state is LoadingMoreWorksListState || state is LoadingWorkListState)   return;

    final int page = state.page + 1;
    emit(LoadingMoreWorksListState(works: state.works, page: page));
    final peopleList = await _workService.getWorks(page: page);

    peopleList.fold(
      (error) => emit(SuccessWorkListState(works: state.works, page: page - 1)),
      (works) => emit(
          SuccessWorkListState(works: [...state.works, ...works], page: page)),
    );
  }

  void _searchWorks(SearchWorksEvent event, Emitter<WorkListState> emit) async {
    if (event.query.isEmpty) {
      emit(LoadingWorkListState(page: 0));
      final workList = await _workService.getWorks();
      workList.fold(
        (error) => emit(ErrorWorkListState(error.message)),
        (works) => emit(SuccessWorkListState(works: works)),
      );
      
      return;
    }

    emit(LoadingWorkListState());

    final peopleList = await _workService.searchWorks(query: event.query);

    peopleList.fold(
      (error) =>
          emit(SuccessWorkListState(works: state.works, page: state.page)),
      (works) => emit(SuccessSearchWorksListState(
        works: works,
      )),
    );
  }

  void _deleteWork(DeleteWorkEvent event,Emitter<WorkListState> emit) async {
    emit(LoadingWorkListState());

    final deletePerson = await _workService.deleteWork(id: event.id);

    deletePerson.fold((left) => emit(ErrorWorkListState(left.message)), (right) => null);

    final workList = await _workService.getWorks();
    workList.fold(
      (error) => emit(ErrorWorkListState(error.message)),
      (works) => emit(SuccessWorkListState(works: works)),
    );
  }
}
