part of 'work_list_bloc.dart';

abstract class WorkListEvent {}

class LoadWorkListEvent extends WorkListEvent {}

class LoadNextPageEvent extends WorkListEvent{}

class SearchWorksEvent extends WorkListEvent{
  final String query;

  SearchWorksEvent(this.query);
}
class DeleteWorkEvent extends WorkListEvent{
  final String id;

  DeleteWorkEvent(this.id);
}
