part of 'people_modal_bloc.dart';

abstract class PeopleModalEvent {}

class SearchPeopleModalEvent extends PeopleModalEvent {
  String query;

  SearchPeopleModalEvent(this.query);
}
