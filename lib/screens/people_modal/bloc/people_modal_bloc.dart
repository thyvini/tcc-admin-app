import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/dtos/responses/person_response.dart';

import '../../../services/people_service.dart';

part 'people_modal_event.dart';

part 'people_modal_state.dart';

class PeopleModalBloc extends Bloc<PeopleModalEvent, PeopleModalState> {
  final PeopleService peopleService;

  PeopleModalBloc({required this.peopleService})
      : super(InitialPeopleModalState()) {
    on<SearchPeopleModalEvent>(
        (event, emit) => _searchPeople(emit, event.query));
  }

  Future<void> _searchPeople(
      Emitter<PeopleModalState> emit, String query) async {
    emit(LoadingPeopleModalState());
    try {
      final peopleResult = await peopleService.getPerson(name: query);
      peopleResult.fold(
        (error) =>
            error.code == '404' ? emit(NoResultsPeopleModalState()) : emit(ErrorPeopleModalState(error.message)),
        (people) => emit(SuccessPeopleModalState(people)),
      );
    } catch (e) {
      emit(ErrorPeopleModalState(e.toString()));
    }
  }
}
