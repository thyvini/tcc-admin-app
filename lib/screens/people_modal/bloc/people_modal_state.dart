part of 'people_modal_bloc.dart';

abstract class PeopleModalState {}

class InitialPeopleModalState extends PeopleModalState {}

class LoadingPeopleModalState extends PeopleModalState {}

class NoResultsPeopleModalState extends PeopleModalState {}

class ErrorPeopleModalState extends PeopleModalState {
  final String message;

  ErrorPeopleModalState(this.message);
}

class SuccessPeopleModalState extends PeopleModalState {
  final PersonResponse person;

  SuccessPeopleModalState(this.person);
}
