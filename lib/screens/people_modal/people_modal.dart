import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/di/dependency_injection.dart';
import 'package:tcc_admin/dtos/responses/work_response.dart';
import 'package:tcc_admin/screens/people_modal/bloc/people_modal_bloc.dart';
import 'package:tcc_admin/widgets/text_form_input.dart';

import '../../dtos/modal_person_dto.dart';
import '../../services/people_service.dart';

class PeopleModal extends StatefulWidget {
  const PeopleModal({Key? key}) : super(key: key);

  @override
  State<PeopleModal> createState() => _PeopleModalState();
}

class _PeopleModalState extends State<PeopleModal> {
  ModalPersonDto? person;

  final TextEditingController nameController = TextEditingController();
  final TextEditingController functionController = TextEditingController();

  Timer? _debounce;
  bool urlError = false;
  bool isMain = false;
  @override
  Widget build(BuildContext context) {
    return BlocProvider<PeopleModalBloc>(
      create: (ctx) =>
          PeopleModalBloc(peopleService: getIt.get<PeopleService>()),
      child:
          BlocBuilder<PeopleModalBloc, PeopleModalState>(builder: (ctx, state) {
        return Dialog(
          backgroundColor: Colors.grey.shade800,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            height: 500,
            width: 500,
            padding: const EdgeInsets.all(25.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text(
                  'Selecionar pessoa',
                  style: TextStyle(
                    fontSize: 40.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Center(
                  child: state is SuccessPeopleModalState
                      ? urlError
                          ? const CircleAvatar(
                              radius: 65.0,
                              child: Center(child: Icon(Icons.person)),
                            )
                          : CircleAvatar(
                              radius: 65.0,
                              backgroundImage:
                                  NetworkImage(state.person.photo, scale: 0.1),
                              onBackgroundImageError: (exception, stackTrace) {
                                setState(() {
                                  urlError = true;
                                });
                              },
                            )
                      : CircleAvatar(
                          backgroundColor: Colors.grey.shade600,
                          radius: 65.0,
                        ),
                ),
                TextFormInput(
                  hintText: 'Nome',
                  controller: nameController,
                  onChanged: (value) {
                    if (_debounce?.isActive ?? false) _debounce?.cancel();
                    _debounce = Timer(
                        const Duration(milliseconds: 1500),
                        () => ctx
                            .read<PeopleModalBloc>()
                            .add(SearchPeopleModalEvent(value ?? '')));
                  },
                  suffixIcon: const Icon(Icons.search, color: Colors.white),
                ),
                TextFormInput(
                  hintText: 'Função',
                  controller: functionController,
                  isRequired: true,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      Checkbox(value: isMain,  onChanged: (value) => setState(() {
                        isMain = value ?? false;
                      })),
                      const SizedBox(width: 10.0),
                      const Text('Principal', style: TextStyle(color: Colors.white, letterSpacing: 1.25, fontSize: 20.0))
                    ],
                  ),
                ),
                OutlinedButton(
                  onPressed: () {
                    if (state is SuccessPeopleModalState) {
                      Navigator.pop(
                          context,
                          WorkResponsePersonDetails(
                            person: WorkResponsePerson(
                              id: state.person.id,
                              photo: state.person.photo,
                              name: nameController.text,
                            ),
                            isMain: isMain,
                            role: functionController.text,
                          ));
                    }
                  },
                  style: OutlinedButton.styleFrom(
                    backgroundColor: Colors.green.shade900,
                  ),
                  child: const Text(
                    'Salvar',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
