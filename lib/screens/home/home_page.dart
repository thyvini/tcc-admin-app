import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/app_router.dart';
import 'package:tcc_admin/screens/people_list/bloc/people_list_bloc.dart';
import 'package:tcc_admin/screens/people_list/people_list_screen.dart';
import 'package:tcc_admin/screens/work_list/bloc/work_list_bloc.dart';
import 'package:tcc_admin/screens/work_list/work_list_screen.dart';

class HomeScreen extends StatelessWidget {
  final int initialIndex;

  const HomeScreen({
    this.initialIndex = 0,
    Key? key,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    Timer? debounce;

    return DefaultTabController(
      length: 2,
      initialIndex: initialIndex,
      child: Builder(builder: (context) {
        return Scaffold(
          backgroundColor: const Color(0xFF2E2E2E),
          appBar: AppBar(            
            leading: Container(),
            toolbarHeight: 75.0,
            backgroundColor: const Color(0xFF5C5C5C),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Expanded(
                  flex: 2,
                  child: TabBar(indicatorColor: Colors.white, tabs: [
                    Tab(
                      child: Text(
                        'Pessoas',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 25.0),
                      ),
                    ),
                    Tab(
                      child: Text(
                        'Obras',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 25.0),
                      ),
                    ),
                  ]),
                ),
                const Spacer(flex: 3),
                Expanded(
                  flex: 5,
                  child: TextField(
                    style: const TextStyle(color: Colors.white),
                    cursorColor: Colors.white,
                    onChanged: (value) {
                      final int tabBar = DefaultTabController.of(context)!.index;
                      if (debounce?.isActive ?? false) debounce?.cancel();
                      debounce = Timer(const Duration(milliseconds: 500), () {
                        if (tabBar == 0) {
                          context
                              .read<PeopleListBloc>()
                              .add(SearchPeopleEvent(value));
                        } else {
                          context
                              .read<WorkListBloc>()
                              .add(SearchWorksEvent(value));
                        }
                      });
                    },
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.search, color: Colors.white),
                      prefixIconColor: Colors.white70,
                      isCollapsed: true,
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 15.0),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.zero,
                        borderSide:
                            BorderSide(width: 2.0, color: Colors.white),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.zero,
                        borderSide:
                            BorderSide(width: 2.0, color: Colors.white70),
                      ),
                      hintText: 'Pesquise...',
                      hintStyle: TextStyle(color: Colors.white60),
                    ),
                  ),
                ),
                const SizedBox(width: 25.0),
                IconButton(
                  onPressed: () {
                    final int tabBar = DefaultTabController.of(context)!.index;
                    if (tabBar == 0) {
                      Navigator.pushNamed(context, AppRouter.maintainPerson);
                    } else {
                      Navigator.pushNamed(context, AppRouter.maintainWork);

                    }
                  },
                  icon: const Icon(Icons.add),
                ),
              ],
            ),
          ),
          body: const TabBarView(
            children: [
              PeopleListScreen(),
              WorkListScreen(),
            ],
          ),
        );
      }),
    );
  }
}
