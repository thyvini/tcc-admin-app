import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/app_router.dart';
import 'package:tcc_admin/constants/helpers.dart';
import 'package:tcc_admin/constants/works.dart';
import 'package:tcc_admin/dtos/responses/work_response.dart';
import 'package:tcc_admin/helpers/custom_scroll_behavior.dart';
import 'package:tcc_admin/helpers/toast.dart';
import 'package:tcc_admin/screens/maintain/bloc_work/work_bloc.dart';
import 'package:tcc_admin/widgets/text_form_input.dart';

import '../people_modal/people_modal.dart';

class MaintainWork extends StatefulWidget {
  const MaintainWork({Key? key}) : super(key: key);

  @override
  State<MaintainWork> createState() => _MaintainWorkState();
}

class _MaintainWorkState extends State<MaintainWork> {
  final TextEditingController workNameController = TextEditingController();
  final TextEditingController workReleaseYearController =
      TextEditingController();
  final TextEditingController workProductionCompanyYearController =
      TextEditingController();
  final TextEditingController gamePublisherYearController =
      TextEditingController();
  final TextEditingController workSynopsisController = TextEditingController();
  final TextEditingController workEpisodeNumberController =
      TextEditingController();
  final TextEditingController workUrlController = TextEditingController();
  final TextEditingController episodeMinDurationController = TextEditingController();
  final TextEditingController workTagsController = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  String? workType;
  String? urlError;
  String? picUrl;

  void _fillValues(WorkResponse? work) {
    if (work != null && workUrlController.text == '') {
      workNameController.text = work.name;

      workReleaseYearController.text = work.releaseYear.toString();

      workType = work.type;

      workTagsController.text = work.tags.join(', ');

      workUrlController.text = work.cover;
      picUrl = work.cover;

      workEpisodeNumberController.text = work.episodeNumber?.toString() ??
          work.minuteDuration?.toString() ??
          work.pageNumber?.toString() ??
          work.developer ??
          '';

      gamePublisherYearController.text =
          workType == 'movie' || workType == 'series'
              ? work.productionCompany ?? ''
              : '';

      episodeMinDurationController.text =  workType == 'series' ? work.minuteDuration?.toString() ?? '': '';

      workSynopsisController.text = work.synopsis ?? '';

      gamePublisherYearController.text = work.publisher ?? '';
    }
  }

  @override
  Widget build(BuildContext context) {
    final ScrollController scrollController = ScrollController();
    return Scaffold(
      backgroundColor: const Color(0xFF2E2E2E),
      body: LayoutBuilder(builder: (context, constraints) {
        return Center(
          child: Container(
            constraints: BoxConstraints(
                maxWidth: constraints.maxWidth * 0.50,
                maxHeight: constraints.maxHeight * 1.2),
            padding: const EdgeInsets.all(12.0),
            margin: const EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              color: Colors.grey.shade900,
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: BlocConsumer<WorkBloc, WorkState>(
              listener: (context, state) {
                if (state is SavedWorkState) {
                  Navigator.pushNamedAndRemoveUntil(
                    context,
                    AppRouter.homeRoute,
                    (_) => false,
                    arguments: 1,
                  );
                }
              },
              builder: (context, state) {
                if (state is LoadingWorkState) {
                  return const Center(child: CircularProgressIndicator());
                }

                _fillValues(state.work);
                return Form(
                  key: formKey,
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Incluir Obra',
                            style: TextStyle(
                              fontSize: 40.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Row(children: [
                            Expanded(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: TextFormInput(
                                          controller: workNameController,
                                          hintText: 'Título',
                                          isRequired: true,
                                        ),
                                      ),
                                      const SizedBox(width: 25.0),
                                      Expanded(
                                        child: TextFormInput(
                                          controller: workReleaseYearController,
                                          formatters: [
                                            FilteringTextInputFormatter
                                                .digitsOnly
                                          ],
                                          hintText: 'Lançamento',
                                          isRequired: true,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 40.0),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: DropdownButtonFormField<String>(
                                          value: workType,
                                          items: workTypes
                                              .map<DropdownMenuItem<String>>(
                                                  (e) => DropdownMenuItem(
                                                      value: e,
                                                      child: Text(
                                                          translateWorkType[
                                                              e])))
                                              .toList(),
                                          onChanged: (value) {
                                            setState(() {
                                              workType = value;
                                            });
                                          },
                                          isDense: true,
                                          style: const TextStyle(
                                            color: Colors.white,
                                            letterSpacing: 1.25,
                                            fontSize: 16.0,
                                          ),
                                          dropdownColor: Colors.grey.shade700,
                                          decoration: const InputDecoration(
                                            contentPadding: EdgeInsets.zero,
                                            helperText: '',
                                            alignLabelWithHint: true,
                                            labelStyle: TextStyle(
                                                color: Colors.white38,
                                                letterSpacing: 1.25),
                                            labelText: 'Tipo',
                                            border: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white),
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white,
                                                  width: 0.5),
                                            ),
                                            focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white,
                                                  width: 1),
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 25.0),
                                      Expanded(
                                        child: TextFormInput(
                                          controller: workEpisodeNumberController,
                                          helperText: '',
                                          formatters: workType != 'game'
                                              ? [
                                                  FilteringTextInputFormatter
                                                      .digitsOnly
                                                ]
                                              : null,
                                          hintText: workType == 'game'
                                              ? 'Desenvolvedor'
                                              : workType == 'movie'
                                                  ? 'Duração'
                                                  : workType == 'book'
                                                      ? 'Nº de págs.'
                                                      : 'Nº de episódios',
                                          isRequired: true,
                                        ),
                                      ),
                                      Visibility(
                                        visible:  workType == 'series',
                                        child: const SizedBox(width: 25.0)),
                                      Visibility(
                                        visible:  workType == 'series',
                                        child: Expanded(
                                          child: TextFormInput(
                                            controller: episodeMinDurationController,
                                            helperText: '',
                                            formatters:[
                                                    FilteringTextInputFormatter
                                                        .digitsOnly
                                                  ],
                                            hintText: 'Duração',
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(width: 25.0),
                            picUrl != null
                                ? Container(
                                    height: 180,
                                    width: 130,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: NetworkImage(picUrl!),
                                        fit: BoxFit.cover,
                                        filterQuality: FilterQuality.none,
                                        onError: (exception, stackTrace) {
                                          setState(() {
                                            picUrl = null;
                                            urlError = 'URL inválida';
                                          });
                                        },
                                      ),
                                      color: Colors.grey.shade300,
                                      borderRadius: BorderRadius.circular(7),
                                    ),
                                  )
                                : Container(
                                    height: 180,
                                    width: 130,
                                    decoration: BoxDecoration(
                                      color: Colors.grey.shade300,
                                      borderRadius: BorderRadius.circular(7),
                                    ),
                                  ),
                          ]),
                          Visibility(
                            visible: workType == 'game' ||
                                workType == 'movie' ||
                                workType == 'series',
                            child: TextFormInput(
                              controller: gamePublisherYearController,
                              hintText: workType == 'game'
                                  ? 'Distribuidor'
                                  : 'Produção',
                            ),
                          ),
                          const SizedBox(height: 15.0),
                          TextFormInput(
                            controller: workUrlController,
                            hintText: 'URL da foto',
                            isRequired: true,
                            errorText: urlError,
                            onChanged: (value) {
                              setState(
                                () {
                                  workUrlController.text = value ?? '';
                                  urlError = null;
                                  picUrl = value;
                                },
                              );
                            },
                          ),
                          const SizedBox(
                            height: 15.0,
                          ),
                          TextFormInput(
                            controller: workTagsController,
                            hintText: 'Tags (separadas por vírgula)',
                            isRequired: true,
                          ),
                          const SizedBox(
                            height: 15.0,
                          ),
                          const Text(
                            'Pessoas',
                            style: TextStyle(
                              fontSize: 32,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 12.0),
                            child: SizedBox(
                              height: 140.0,
                              child: ScrollConfiguration(
                                behavior: CustomScrollBehaviour(),
                                child: Scrollbar(
                                  thumbVisibility: true,
                                  controller: scrollController,
                                  interactive: true,
                                  child: ListView.separated(
                                    controller: scrollController,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: state.people.length + 1,
                                    separatorBuilder: ((context, index) {
                                      return const SizedBox(width: 24);
                                    }),
                                    itemBuilder: ((context, index) {
                                      if (index == 0) {
                                        return InkWell(
                                          onTap: () async {
                                            final workBloc =
                                                context.read<WorkBloc>();
                                            final WorkResponsePersonDetails?
                                                personFromDialog =
                                                await showDialog<
                                                        WorkResponsePersonDetails?>(
                                                    context: context,
                                                    builder: (dCtx) =>
                                                        const PeopleModal());

                                            if (personFromDialog != null) {
                                              workBloc.add(AddPersonToListEvent(
                                                  personFromDialog));
                                            }
                                          },
                                          child: Column(
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Colors.transparent,
                                                    border: Border.all(
                                                        color: Colors.grey)),
                                                constraints:
                                                    const BoxConstraints(
                                                  minHeight: 55.0,
                                                  maxHeight: 55.0,
                                                  minWidth: 55.0,
                                                  maxWidth: 55.0,
                                                ),
                                                child: const Center(
                                                  child: Icon(
                                                    Icons.add,
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(height: 10.0,),
                                              const SizedBox(
                                                width: 70,
                                                child: Text(
                                                  'Adicionar pessoa',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      }

                                      final person = state.people[index - 1];
                                      final personWidget = Container(
                                        padding: const EdgeInsets.all(5.0),
                                        decoration:  person.isMain ?? false ? BoxDecoration(
                                          borderRadius: BorderRadius.circular(10.0),
                                          border: Border.all(color: Colors.white, width: 1),
                                          color: Colors.grey.shade900
                                        ) : null,
                                        child: 
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              CircleAvatar(
                                                  radius: 25.0,
                                                  foregroundImage: NetworkImage(
                                                      person.person.photo),
                                                  child: const Icon(Icons.person)),
                                              const SizedBox(height: 9.0),
                                              Text(person.person.name,
                                                  textAlign: TextAlign.center,
                                                  style: const TextStyle(
                                                      fontSize: 13,
                                                      color: Colors.white)),
                                              const SizedBox(height: 7.0),
                                              Expanded(
                                                child: Text(person.role,
                                                    textAlign: TextAlign.center,
                                                    style: const TextStyle(
                                                        fontSize: 12,
                                                        color: Colors.white70)),
                                              ),
                                            ],
                                          ),                                        
                                      );

                                      bool isHovered = false;
                                      return StatefulBuilder(
                                        builder: (context, setPersonState) {

                                          return MouseRegion(
                                            cursor: SystemMouseCursors.click,
                                            onEnter: (_) => setPersonState(() => isHovered = true),
                                            onExit: (_) => setPersonState(() => isHovered = false),
                                            child: GestureDetector(
                                              onTap: () => showDialog(
                                                  context: context,
                                                  builder: (BuildContext ctx) {
                                                    return AlertDialog(
                                                      title: const Text(
                                                          'Confirmar remoção'),
                                                      content: Text(
                                                          'Tem certeza que deseja remover ${person.person.name} da obra?'),
                                                      actions: [
                                                        TextButton(
                                                          onPressed: () {
                                                            context
                                                                .read<WorkBloc>()
                                                                .add(
                                                                    RemovePersonFromListEvent(
                                                                        person));
                                                            Navigator.of(ctx).pop();
                                                          },
                                                          child: const Text('Sim'),
                                                        ),
                                                        TextButton(
                                                          onPressed: () =>
                                                              Navigator.of(ctx)
                                                                  .pop(),
                                                          child: const Text('Não'),
                                                        )
                                                      ],
                                                    );
                                                  }),
                                              child: SizedBox(
                                                height: 85.0,
                                                width: 90,
                                                child: !isHovered
                                                    ? personWidget
                                                    : Stack(
                                                        alignment:
                                                            Alignment.topCenter,
                                                        children: [
                                                          personWidget,
                                                          Padding(
                                                            padding: const EdgeInsets.all(5.0),
                                                            child: CircleAvatar(
                                                              radius: 25,
                                                              backgroundColor:
                                                                  Colors.red.withOpacity(0.5),
                                                              child:
                                                                  const Icon(Icons.remove),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                              ),
                                            ),
                                          );
                                        }
                                      );
                                    }),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          TextFormField(
                            controller: workSynopsisController,
                            maxLines: 15,
                            minLines: 7,
                            style: const TextStyle(
                              color: Colors.white,
                              letterSpacing: 1.25,
                              fontSize: 18.0,
                            ),
                            cursorColor: Colors.white,
                            cursorWidth: 1.0,
                            cursorHeight: 20,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return ('Campo Obrigatório');
                              }
                            },
                            decoration: InputDecoration(
                              hintStyle: const TextStyle(
                                  color: Colors.white38, letterSpacing: 1.25),
                              hintText: 'Sinópse',
                              focusedBorder: const OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.white, width: 1.0),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).errorColor),
                              ),
                              enabledBorder: const OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.white, width: 0.5),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              OutlinedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                style: OutlinedButton.styleFrom(
                                  backgroundColor: Colors.red.shade900,
                                ),
                                child: const Text(
                                  'Cancelar',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              OutlinedButton(
                                onPressed: () {                                  
                                  if (formKey.currentState?.validate() ?? false) {
                                    if(state.people.isEmpty){
                                      showToast(message: 'Inclua pelo menos uma pessoa na obra.');
                                      return;
                                    }

                                    final tags = workTagsController.text
                                        .split(',')
                                        .map((e) => e.trim())
                                        .toList();
                                    final payload = WorkResponse(
                                      id: state.work?.id ?? '',
                                      name: workNameController.text,
                                      cover: workUrlController.text,
                                      type: workType!,
                                      tags: tags,
                                      averageScore: state.work?.averageScore ?? 0,
                                      people: state.people,                                      
                                      publisher: workType == 'game'
                                          ? gamePublisherYearController.text
                                          : null,
                                      developer: workType == 'game'
                                          ? workEpisodeNumberController.text
                                          : null,
                                      episodeNumber: workType == 'series'
                                          ? int.tryParse(
                                              workEpisodeNumberController.text)
                                          : null,
                                      minuteDuration: workType == 'movie'
                                          ? int.tryParse(
                                              workEpisodeNumberController.text)
                                          : workType == 'series'
                                          ? int.tryParse(episodeMinDurationController.text) : null,
                                      pageNumber: workType == 'book'
                                          ? int.tryParse(
                                              workEpisodeNumberController.text)
                                          : null,
                                      productionCompany: workType == 'movie' ||
                                              workType == 'series'
                                          ? gamePublisherYearController.text
                                          : null,
                                      releaseYear: int.tryParse(
                                          workReleaseYearController.text),
                                      synopsis: workSynopsisController.text,
                                    );

                                    context
                                        .read<WorkBloc>()
                                        .add(SaveWorkEvent(payload));
                                  }
                                },
                                style: OutlinedButton.styleFrom(
                                  backgroundColor: Colors.green.shade900,
                                ),
                                child: const Text(
                                  'Salvar',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        );
      }),
    );
  }
}