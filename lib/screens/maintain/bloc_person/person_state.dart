part of 'person_bloc.dart';

abstract class PersonState{
}

class PersonLoadingState extends PersonState {}

class PersonReadyState extends PersonState {
  final PersonResponse? person;

  PersonReadyState({this.person});
}

class PersonSavedState extends PersonState {}

class PersonErrorState extends PersonState {}
