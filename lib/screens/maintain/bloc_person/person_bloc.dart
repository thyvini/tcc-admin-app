import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/di/dependency_injection.dart';
import 'package:tcc_admin/dtos/requests/person_request.dart';
import 'package:tcc_admin/dtos/responses/person_response.dart';
import 'package:tcc_admin/services/people_service.dart';

part 'person_event.dart';
part 'person_state.dart';

class PersonBloc extends Bloc<PersonEvent, PersonState> {
  late final PeopleService _peopleService;

  PersonBloc({
    String? name,
  }) : super(PersonLoadingState()) {
    _peopleService = getIt.get<PeopleService>();

    on<SavePersonEvent>((event, emit) => _addPerson(event, emit));
    on<LoadPersonEvent>((event, emit) => _loadPerson(event, emit));
    add(LoadPersonEvent(name: name));
  }

  void _addPerson(SavePersonEvent event, Emitter<PersonState> emit) async {
    PersonRequest person = event.person;

    emit(PersonLoadingState());
    final response = await _peopleService.savePeople(person: person);

    response.fold(
      (left) => emit(PersonErrorState()),
      (right) => emit(PersonSavedState()),
    );
  }

  void _loadPerson(LoadPersonEvent event, Emitter<PersonState> emit) async {
    emit(PersonLoadingState());
    if (event.name != null) {
      final response = await _peopleService.getPerson(name: event.name!);

      response.fold(
        (left) => emit(PersonErrorState()),
        (person) => emit(PersonReadyState(person: person)),
      );
    } else {
      emit(PersonReadyState());
    }
  }
}
