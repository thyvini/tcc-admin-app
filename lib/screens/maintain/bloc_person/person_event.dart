part of 'person_bloc.dart';

abstract class PersonEvent {}

class DeletePersonEvent extends PersonEvent {
   final String? id;

  DeletePersonEvent({this.id});
}

class SavePersonEvent extends PersonEvent {
  final PersonRequest person;

  SavePersonEvent({required this.person});
}

class LoadPersonEvent extends PersonEvent {
  final String? name;

  LoadPersonEvent({this.name});
}
