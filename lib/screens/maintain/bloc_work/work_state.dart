part of 'work_bloc.dart';

abstract class WorkState {
  final WorkResponse? work;
  final List<WorkResponsePersonDetails> people;
  WorkState({this.people = const [], this.work});
}

class WorkInitial extends WorkState {}

class ReadyWorkState extends WorkState {
  ReadyWorkState({super.people, super.work});
}

class LoadingWorkState extends WorkState {}

class SavedWorkState extends WorkState{}

class ErrorWorkState extends WorkState{}