
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/di/dependency_injection.dart';
import 'package:tcc_admin/dtos/responses/work_response.dart';
import 'package:tcc_admin/services/work_service.dart';

part 'work_event.dart';
part 'work_state.dart';

class WorkBloc extends Bloc<WorkEvent, WorkState> {
  late final WorkService _workService;


  WorkBloc({String? workId}) : super(WorkInitial()) {
    _workService = getIt.get<WorkService>();

    on<AddPersonToListEvent>((event, emit) => _addToList(event, emit));
    on<RemovePersonFromListEvent>((event, emit) => _removeFromList(event, emit));
    on<SaveWorkEvent>((event, emit) => _saveWork(event, emit));
    on<LoadWorkEvent>((event, emit) => _loadWork(event, emit));

    if(workId !=null){
      add(LoadWorkEvent(workId));
    }
  }

  void _addToList(AddPersonToListEvent event, Emitter<WorkState> emit) {
    if (!state.people.any((element) => element.person.name == event.person.person.name)) {
      emit(ReadyWorkState(people: [...state.people, event.person], work: state.work));
    }
  }

  void _removeFromList(RemovePersonFromListEvent event, Emitter<WorkState> emit) {
    final peopleList = state.people;

    peopleList.removeWhere((element) => element.person.name == event.person.person.name);
    emit(ReadyWorkState(people: [...peopleList], work: state.work));
  }

  void _saveWork(SaveWorkEvent event, Emitter<WorkState> emit)async {
    final work = event.work.copyWith(
      id: state.work?.id ?? '',
    );

    emit(LoadingWorkState());

    final response = await _workService.saveWork(work: work);

    response.fold(
      (left) => emit(ErrorWorkState()),
      (right) => emit(SavedWorkState()),
    );
  }

  void _loadWork(LoadWorkEvent event, Emitter<WorkState> emit)async {
    emit(LoadingWorkState());

    final response = await _workService.getWork(id: event.id);

    response.fold(
      (left) => emit(ErrorWorkState()),
      (work) => emit(ReadyWorkState(people: work.people, work: work)),
    );
  }
}
