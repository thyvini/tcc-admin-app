part of 'work_bloc.dart';

abstract class WorkEvent {
}

class AddPersonToListEvent extends WorkEvent {
  final WorkResponsePersonDetails person;

  AddPersonToListEvent(this.person);
}
class RemovePersonFromListEvent extends WorkEvent {
  final WorkResponsePersonDetails person;

  RemovePersonFromListEvent(this.person);
}

class SaveWorkEvent extends WorkEvent {
  final WorkResponse work;
  SaveWorkEvent(this.work);
}

class LoadWorkEvent extends WorkEvent {
  final String id;
  LoadWorkEvent(this.id);
}