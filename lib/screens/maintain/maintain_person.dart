import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:tcc_admin/app_router.dart';
import 'package:tcc_admin/dtos/requests/person_request.dart';
import 'package:tcc_admin/dtos/responses/person_response.dart';
import 'package:tcc_admin/helpers/string_helpers.dart';
import 'package:tcc_admin/screens/maintain/bloc_person/person_bloc.dart';
import 'package:tcc_admin/widgets/text_form_input.dart';

class MaintainPerson extends StatefulWidget {
  MaintainPerson({Key? key}) : super(key: key);

  @override
  State<MaintainPerson> createState() => _MaintainPersonState();
}

class _MaintainPersonState extends State<MaintainPerson> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextEditingController nameController = TextEditingController();

  final TextEditingController picUrlController = TextEditingController();

  final TextEditingController birthdayController = TextEditingController();

  final TextEditingController bioController = TextEditingController();

  final maskFormatter = MaskTextInputFormatter(
    mask: '##/##/####',
    filter: {"#": RegExp(r'[0-9]')},
  );

  late final FocusNode bioFocus;

  String? picUrl;
  String? urlError;

  @override
  void initState() {
    bioFocus = FocusNode()
      ..addListener(() {
        if (bioFocus.hasFocus) {
          setState(
            () {
              urlError = null;
              picUrl = picUrlController.text;
            },
          );
        }
      });
    super.initState();
  }

  _fillFields(PersonResponse? person) {
    if (person != null) {
      nameController.text = person.name;
      picUrlController.text = person.photo;

      setState(() {
        picUrl = picUrlController.text;
      });
      if (person.biography != null) {
        bioController.text = person.biography!;
      }
      if (person.birthDate != null) {
        birthdayController.text = formatDateToUi(person.birthDate!);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF2E2E2E),
      body: Center(
        child: Container(
          constraints: const BoxConstraints.tightFor(width: 646, height: 650),
          padding: const EdgeInsets.all(35.0),
          decoration: BoxDecoration(
            color: Colors.grey.shade900,
            borderRadius: BorderRadius.circular(33.0),
          ),
          child: BlocConsumer<PersonBloc, PersonState>(
            listener: (context, state) {
              if (state is PersonSavedState) {
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  AppRouter.homeRoute,
                  (_) => false,
                );
              }
              if (state is PersonReadyState) {
                _fillFields((state).person);
              }
            },
            builder: (context, state) {
              if (state is PersonLoadingState) {
                return const Center(child: CircularProgressIndicator());
              }

              return Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    const Text(
                      'Incluir Pessoa',
                      style: TextStyle(
                        fontSize: 40.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Center(
                      child: picUrl != null && picUrl!.isNotEmpty
                          ? CircleAvatar(
                              radius: 65.0,
                              backgroundImage:
                                  NetworkImage(picUrl!, scale: 0.1),
                              onBackgroundImageError: (exception, stackTrace) {
                                setState(() {
                                  picUrl = null;
                                  urlError = 'URL inválida';
                                });
                              },
                            )
                          : CircleAvatar(
                              backgroundColor: Colors.grey.shade600,
                              radius: 65.0,
                            ),
                    ),
                    TextFormInput(
                      controller: nameController,
                      hintText: 'Nome',
                      isRequired: true,
                    ),
                    TextFormInput(
                      controller: birthdayController,
                      hintText: 'Data de Nascimento',
                      formatters: [maskFormatter],
                      isRequired: true,
                    ),
                    TextFormInput(
                      controller: picUrlController,
                      hintText: 'URL da foto',
                      isRequired: true,
                      errorText: urlError,
                    ),
                    TextFormField(
                      controller: bioController,
                      focusNode: bioFocus,
                      maxLines: 15,
                      minLines: 7,
                      style: const TextStyle(
                        color: Colors.white,
                        letterSpacing: 1.25,
                        fontSize: 18.0,
                      ),
                      cursorColor: Colors.white,
                      cursorWidth: 1.0,
                      cursorHeight: 20,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return ('Campo Obrigatório');
                        }
                      },
                      decoration: InputDecoration(
                        hintStyle: const TextStyle(
                            color: Colors.white38, letterSpacing: 1.25),
                        hintText: 'Biografia',
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade600),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Theme.of(context).errorColor),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade600),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        OutlinedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          style: OutlinedButton.styleFrom(
                            backgroundColor: Colors.red.shade900,
                          ),
                          child: const Text(
                            'Cancelar',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        OutlinedButton(
                          onPressed: () {
                            if (formKey.currentState?.validate() ?? false) {
                              context.read<PersonBloc>().add(SavePersonEvent(
                                  person: PersonRequest(
                                      id: (state as PersonReadyState).person?.id,
                                      name: nameController.text,
                                      biography: bioController.text,
                                      birthDate: formatDateToApi(
                                          birthdayController.text),
                                      photo: picUrlController.text),),);
                            }
                          },
                          style: OutlinedButton.styleFrom(
                            backgroundColor: Colors.green.shade900,
                          ),
                          child: const Text(
                            'Salvar',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
