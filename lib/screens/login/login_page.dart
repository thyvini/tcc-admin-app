import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/bloc/session_bloc.dart';
import 'package:tcc_admin/app_router.dart';
import 'package:tcc_admin/widgets/main_button.dart';
import 'package:tcc_admin/widgets/text_form_input.dart';

class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _userController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade800,
      body: Center(
          child: Container(
        constraints: const BoxConstraints.tightFor(width: 646, height: 508),
        decoration: BoxDecoration(
          color: Colors.grey.shade900,
          borderRadius: BorderRadius.circular(33.0),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 160.0, vertical: 80.0),
        child: BlocConsumer<SessionBloc, SessionState>(
          listener: (context, state) {
            if(state is SessionUpState){
              Navigator.pushNamedAndRemoveUntil(context, AppRouter.homeRoute, (_) => false);
            }
          },
          builder: (context, state) {
            if(state is SessionLoadingState){
              return const CircularProgressIndicator();
            }if(state is SessionUpState){
              return Container();
            }
            return Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 54.0,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextFormInput(
                    controller: _userController,
                    hintText: 'username',
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Campo Obrigatório';
                      }
                      return null;
                    },
                  ),
                  TextFormInput(
                      controller: _passwordController,
                      hintText: 'senha',
                      obscureText: true,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Campo Obrigatório';
                        }
                        if (value.length > 8) {
                          return 'Senha deve ser maior que 8 carácteres';
                        }
                        return null;
                      }),
                  MainButton(
                    child: const Text(
                      'Entrar',
                      style: TextStyle(
                          fontSize: 24.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    onTap: () {
                      if (_formKey.currentState!.validate()) {
                        context.read<SessionBloc>().add(
                              LoginUserEvent(
                                password: _passwordController.text,
                                username: _userController.text,
                              ),
                            );
                      }
                    },
                  ),
                ],
              ),
            );
          },
        ),
      )),
    );
  }
}
