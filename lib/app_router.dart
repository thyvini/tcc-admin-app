import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tcc_admin/screens/home/home_page.dart';
import 'package:tcc_admin/screens/login/login_page.dart';
import 'package:tcc_admin/screens/maintain/bloc_person/person_bloc.dart';
import 'package:tcc_admin/screens/maintain/bloc_work/work_bloc.dart';
import 'package:tcc_admin/screens/maintain/maintain_person.dart';
import 'package:tcc_admin/screens/people_list/bloc/people_list_bloc.dart';
import 'package:tcc_admin/screens/work_list/bloc/work_list_bloc.dart';

import 'screens/maintain/maintain_work.dart';

class AppRouter {
  static Map<String, Widget Function(BuildContext)> routes = {
    root: (context) => LoginPage(),
    homeRoute: (context) {
      final int? index = ModalRoute.of(context)?.settings.arguments as int?;

      return MultiBlocProvider(
        providers: [
          BlocProvider<WorkListBloc>(
            create: (BuildContext context) => WorkListBloc(),
          ),
          BlocProvider<PeopleListBloc>(
            create: (BuildContext context) => PeopleListBloc(),
          ),
        ],
        child: HomeScreen(
          initialIndex: index ?? 0,
        ),
      );
    },
    maintainPerson: (context) {
      final String? name =
          ModalRoute.of(context)?.settings.arguments as String?;

      return BlocProvider(
          create: (context) => PersonBloc(
                name: name,
              ),
          child: MaintainPerson());
    },
    maintainWork: (context) {
       final String? id =
          ModalRoute.of(context)?.settings.arguments as String?;

      return BlocProvider(
        create: (context) => WorkBloc(
          workId: id,
        ),
        child: MaintainWork(),
      );
    }
  };

  static const String root = '/';
  static const String homeRoute = '/home-page';
  static const String maintainPerson = '/maintain-person-page';
  static const String maintainWork = '/maintain-work-page';
  static const String searchPeople = '/search-people';
}
