# PAIMON - Admin

Projeto de gerenciamento via Web para o sistema PAIMON. Este projeto é executado no navegador, como um sistema Web.

## Requisitos

Para executar ou compilar o projeto, é necessário ter instalada a SDK do Flutter na versão 3. 

## Executando

Para executar o projeto, basta utilizar o comando:

```
flutter run -d chrome
```

Caso haja problema de CORS, é necessário alterar a configuração do Flutter. Para isso, deve-se navegar até o diretório
da SDK, e editar o arquivo `packages/flutter_tools/lib/src/web/chrome.dart`. Nele, basta comentar a linha 
`'--disable-extensions'`, e abaixo dela, adicionar `'--disable-web-security'`.